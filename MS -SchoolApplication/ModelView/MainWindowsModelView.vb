﻿Imports System.ComponentModel

Public Class MainWindowsModelView
    'Interfaces  
    Implements  INotifyPropertyChanged, ICommand


    Private _Modelo as MainWindowsModelView

    Public  Sub New ()
        Me.Modelo = Me
    End Sub
    
    Public Property Modelo as MainWindowsModelView
    Get
            Return _Modelo
    End Get
        Set(value as MainWindowsModelView)
            _Modelo = value
                    'Aqui si va porque nos indicara que vista se obtendra
            NotificarCambio("Modelo")
    
        End Set
    End Property




    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        if parameter = "Department"
            Dim WindowsDepartment as New DepartmentView()
            WindowsDepartment.ShowDialog()
        End If

        If parameter = "Person"
            Dim WindowsPerson As New PersonView
            WindowsPerson.ShowDialog()
        End If

        If parameter = "Course"
            Dim WindowsCourse as New CourseView
            WindowsCourse.ShowDialog()
        End If

        If parameter = "OfficeAssignment"
            Dim WindowsOffice As  New OfficeAssignmentView
            WindowsOffice.ShowDialog()
        End If

        If parameter = "OnlineCourse"
            Dim WindowsOnline as new OnlineCourseView
            WindowsOnline.ShowDialog()
        End If

        if parameter = "OnsiteCourse"
            Dim WindowsOnsite as New  OnsiteCourseView
            WindowsOnsite.ShowDialog()
        End If

        If parameter = "Student"
            Dim WindowsStudent as New StudentGradeView
            WindowsStudent.ShowDialog()
        End If

        If parameter = "CourseInstructor"
            Dim WindowsCourseInstructor As New CourseInstructorView
            WindowsCourseInstructor.ShowDialog()
        End If
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        'Si es false no se excuta lo de Excecute
        Return True
        
    End Function

    Public Sub  NotificarCambio(ByVal Propiedad As String  )
        RaiseEvent PropertyChanged(Me,New PropertyChangedEventArgs(Propiedad))
    End Sub
End Class
