﻿Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Drawing
Imports MS__SchoolApplication.MSSchoolApplication.JimyDiaz.Model
Imports  Tulpep.NotificationWindow
Public Class OnlineCourseModelView
    Implements INotifyPropertyChanged, ICommand, IDataErrorInfo

    #Region "Campos"
    Private _CourseID as integer    
    Private _URL as String
    Private _Course as Course
    Private _OnlineCourse As OnlineCourse
    Private _Guardar as Boolean = False
    Private _Nuevo as Boolean = true
    Private _Actualizar As Boolean = True
    Private _Eliminar as Boolean = True
    Private _Cancelar as Boolean = False
    Private _Combobox as Boolean = true 
    Private _ListOnlineCourses as new List(Of OnlineCourse)
    Private _ListCourses as new List(Of Course)
    Private  _Modelo as  OnlineCourseModelView
    Private DB  As New MS_SchoolApplicationDataContext
    Dim notificacion As New PopupNotifier
 #End Region 

#Region "Propiedades"
    Public  Property CourseID() As Integer
    Get
            Return _CourseID
    End Get
        Set(value As Integer)
            _CourseID = value
            NotificarCambio("CourseID")
        End Set
    End Property

    Public  Property URL() As String
    Get
            Return _URL
    End Get
        Set(value As String)
            _URL =value
            NotificarCambio("URL")
        End Set
    End Property

    Public Property Course() As Course
    Get
            Return _Course
    End Get
        Set(value As Course)
            _Course = value
            NotificarCambio("Course")
        End Set
    End Property
    Public Property Combobox() As Boolean
    Get
            Return _Combobox
    End Get
        Set(value As Boolean)
            _Combobox = value
            NotificarCambio("Combobox")
        End Set
    End Property

    Public Property OnlineCourse as OnlineCourse
    Get
            Return _OnlineCourse
    End Get
        Set(value as OnlineCourse)
            _OnlineCourse = value   
            NotificarCambio("OnlineCourse")
            if value IsNot Nothing
                Me.Course = _OnlineCourse.Course
                Me.URL = _OnlineCourse.URL
            End If
        End Set
    End Property

    Public  Property Modelo() As OnlineCourseModelView
    Get
            Return _Modelo
    End Get
        Set(value As OnlineCourseModelView)
            _Modelo = value 
        End Set
    End Property
    Public  Property Guardar() As Boolean
    Get
            Return _Guardar
    End Get
        Set(value As Boolean)
            _Guardar  = value   
            NotificarCambio("Guardar")
        End Set
    End Property

    Public Property Nuevo() As Boolean
    Get
            Return _Nuevo
    End Get
        Set(value As Boolean)
            _Nuevo = value  
            NotificarCambio("Nuevo")
        End Set
    End Property
    Public Property Actualizar() As Boolean
    Get
            Return _Actualizar
    End Get
        Set(value As Boolean)
            _Actualizar = value 
            NotificarCambio("Actualizar")
        End Set
    End Property
    Public  Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value   
            NotificarCambio("Eliminar")
        End Set
    End Property
    Public  Property Cancelar() As Boolean
    Get
            Return   _Cancelar
    End Get
        Set(value As Boolean)
            _Cancelar = value   
            NotificarCambio("Cancelar")
        End Set
    End Property
    Public Property ListOnlineCourses() As List(Of OnlineCourse)
    Get
            If _ListOnlineCourses.Count = 0
                _ListOnlineCourses = (From O in DB.OnlineCourses Select O).ToList()
            End If

            Return _ListOnlineCourses
    End Get
        Set(value As List(Of OnlineCourse))
            _ListOnlineCourses = value  
            NotificarCambio("ListOnlineCourses")
        End Set
    End Property
    Public Property ListCourses() As List(Of Course)
    Get
            If _ListCourses.Count = 0
                _ListCourses = (From C in DB.Courses Select C).ToList()
            End If
            Return _ListCourses
    End Get
        Set(value As List(Of Course))
            _ListCourses = value
            NotificarCambio("ListCourses")
        End Set
    End Property

    Public  Sub New ()
        Me.Modelo = Me
    End Sub

    Public Sub LimpiarControles
        Me.Course = Nothing
        Me.URL = ""
        Me.OnlineCourse = Nothing
    End Sub


#End Region

     Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged


    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        if parameter = "nuevo"
            Me.Nuevo = False
            Me.Guardar = True
            Me.Actualizar = False
            Me.Eliminar = False
            Me.Cancelar = True
            Me.Combobox = True
            LimpiarControles()
        End If


        if parameter = "guardar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Actualizar = True
            Me.Eliminar = True  
            Me.Cancelar = True  
            Me.Combobox = True
            Dim Nuevo as New OnlineCourse
            Nuevo.Course = Me.Course
            Nuevo.URL = Me.URL
            DB.OnlineCourses.Add(Nuevo)
           
            DB.SaveChanges()
            ListOnlineCourses = (From O in DB.OnlineCourses Select O).ToList()
            notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
            notificacion.TitleText = "Registro Realizado!"
            notificacion.ContentText = "El registro se ha realizaaod correctamente"
            notificacion.Popup()
        End If

        If parameter = "actualizar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Eliminar = True
            Me.Combobox = False  
            Me.Actualizar = True
            Me.Cancelar = False
            If OnlineCourse IsNot Nothing
              Me.OnlineCourse.URL = Me.URL
              DB.Entry(OnlineCourse).State = EntityState.Modified
                Db.SaveChanges()
              ListOnlineCourses = (From O in DB.OnlineCourses Select O).ToList()
              notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
              notificacion.TitleText = "Registro Actualizado!"
              notificacion.ContentText = "El registro se ha actualizado correctamente"
              notificacion.Popup()
            End If
        End If


        if parameter = "eliminar"
            if OnlineCourse IsNot Nothing
                Dim Resultado as MsgBoxResult = MsgBoxResult.No
                Resultado = MsgBox("¿Está seguro de eliminar este elemento?", MsgBoxStyle.Question +MsgBoxStyle.YesNo +MsgBoxStyle.DefaultButton2, "Eliminar")
                If Resultado = MsgBoxResult.Yes
                    DB.OnlineCourses.Remove(OnlineCourse)
                    DB.SaveChanges()
                    ListOnlineCourses = (From O in DB.OnlineCourses Select O).ToList()
                    LimpiarControles()
                    notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                    notificacion.TitleText = "Registro Eliminaod!"
                    notificacion.ContentText = "El registro se ha eliminado correctamente"
                    notificacion.Popup()
                End If
            End If
        End If


        if parameter = "cancelar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Eliminar = True
            Me.Combobox = True
            Me.Actualizar = True
            Me.Cancelar = False
            LimpiarControles()
        End If

    End Sub

    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me,New PropertyChangedEventArgs(Propiedad))
    End Sub
End Class
