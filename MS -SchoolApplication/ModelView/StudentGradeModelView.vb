﻿Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Drawing
Imports MS__SchoolApplication.MSSchoolApplication.JimyDiaz.Model
Imports Tulpep.NotificationWindow

Public Class StudentGradeModelView
    Implements INotifyPropertyChanged,IDataErrorInfo, ICommand
    #Region "Campos"
    Private _Person as Person
    Private _Course as Course
    Private _Grade as Integer
    Private _Nuevo as Boolean = True    
    Private _Guardar as Boolean = False
    Private _Eliminar as Boolean =  True
    Private  _Actualizar as Boolean = True
    Private _Cancelar as Boolean = False
    Private _ListPersons as New List(Of Person)
    Private _ListCourses as New List(Of Course)

    Private _ListStudentGrades as New List(Of StudentGrade)

    Private _StudentGrade as StudentGrade
    Private _Modelo as StudentGradeModelView

    Private DB as  New MS_SchoolApplicationDataContext 
    Dim notificacion as new PopupNotifier

#End Region
    #Region "Propiedades"
    Public Property Person() As Person
    Get
            Return _Person
    End Get
        Set(value As Person)
            _Person = value
            NotificarCambio("Person")
        End Set
    End Property
    Public Property Course() As Course
    Get
            Return _Course
    End Get
        Set(value As Course)
            _Course = value 
            NotificarCambio("Course")
        End Set
    End Property
    Public  Property Grade() As Integer
    Get
            Return _Grade
    End Get
        Set(value As Integer)
            _Grade = value
            NotificarCambio("Grade")
        End Set
    End Property
    Public  Property Nuevo() As Boolean
    Get
            Return _Nuevo
    End Get
        Set(value As Boolean)
            _Nuevo = value
            NotificarCambio("Nuevo")
        End Set
    End Property
    Public Property Guardar() As Boolean
    Get
            Return _Guardar
    End Get
        Set(value As Boolean)
            _Guardar = value
            NotificarCambio("Guardar")
        End Set
    End Property
    Public  Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value
            NotificarCambio("Eliminar")
        End Set
    End Property
    Public  Property Actualizar() As Boolean
    Get
            Return _Actualizar
    End Get
        Set(value As Boolean)
            _Actualizar = value
            NotificarCambio("Actualizar")
        End Set
    End Property
    Public  Property Cancelar() As Boolean
    Get
            Return _Cancelar
    End Get
        Set(value As Boolean)
            _Cancelar = value   
            NotificarCambio("Cancelar")
        End Set
    End Property
    Public Property ListPersons() As List(Of Person)
    Get
            If _ListPersons.Count = 0 
                _ListPersons = (From P in DB.Persons Select P).ToList()
            End If
            Return _ListPersons
    End Get 
        Set(value As List(Of Person))
            _ListPersons = value    
            NotificarCambio("ListPersons")
        End Set
    End Property
    Public Property ListCourses() As List(Of Course)
    Get
           If _ListCourses.Count = 0
                _ListCourses = (From C in DB.Courses Select C).ToList()
           End If
            Return _ListCourses
    End Get
        Set(value As List(Of Course))
            _ListCourses =  value
            NotificarCambio("ListCourses")
        End Set
    End Property
    Public Property ListStudentGrades() As List(Of StudentGrade)
        Get
            If _ListStudentGrades.Count = 0
                _ListStudentGrades = (From S IN DB.StudentGrades Select S).ToList()
            End If
            Return _ListStudentGrades
    End Get
        Set(value As List(Of StudentGrade))
            _ListStudentGrades = value
            NotificarCambio("ListStudentGrades")
        End Set
    End Property
    Public Property Modelo as StudentGradeModelView
    Get
            Return _Modelo
    End Get
        Set(value as StudentGradeModelView)
            _Modelo = value
            NotificarCambio("Modelo")
        End Set
    End Property

    Public Property StudentGrade() As StudentGrade
    Get
            Return _StudentGrade
    End Get
        Set(value As StudentGrade)
            _StudentGrade = value
            NotificarCambio("StudentGrade")
            If value  IsNot Nothing
                Me.Course = _StudentGrade.Course
                Me.Person = _StudentGrade.Person
                Me.Grade = _StudentGrade.Grade
            End If
        End Set
    End Property


    Public Sub New ()
        Me.Modelo = Me
    End Sub

    Public  Sub LimpiarControles()
        Me.Course = Nothing
        Me.Person = Nothing
        Me.Grade = 0
        Me.StudentGrade = Nothing
    End Sub
#End Region


    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        If parameter = "nuevo"
            Me.Nuevo = False
            Me.Guardar = True
            Me.Eliminar = False
            Me.Actualizar = False
            Me.Cancelar = True
            LimpiarControles()
        End If

        If parameter = "guardar"
            Me.Nuevo = True
            Me.Guardar = False  
            Me.Actualizar = True
            Me.Eliminar = True  
            Me.Cancelar = False
            Dim Nuevo as new StudentGrade
           Nuevo.Course = Me.Course
            Nuevo.Person = Me.Person
            Nuevo.Grade = Me.Grade
            DB.StudentGrades.Add(Nuevo)
            DB.SaveChanges()
            ListStudentGrades = (From S in DB.StudentGrades Select S).ToList()
            notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
            notificacion.TitleText = "Registro Realizado!"
            notificacion.ContentText = "El registro se ha realizaaod correctamente"
            notificacion.Popup()
        End If

        If parameter = "actualizar"
             Me.Nuevo = True
            Me.Guardar = False
            Me.Eliminar = True
            Me.Actualizar = True
            Me.Cancelar = False
            If StudentGrade IsNot Nothing
                Me.StudentGrade.Course = Me.Course
                Me.StudentGrade.Person = Me.Person
                Me.StudentGrade.Grade = Me.Grade
                DB.Entry(StudentGrade).State = EntityState.Modified
                DB.SaveChanges()
                ListStudentGrades = (From S in DB.StudentGrades Select S).ToList()
                notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                notificacion.TitleText = "Registro Actualizado!"
                notificacion.ContentText = "El registro se ha actualizado correctamente"
                notificacion.Popup()
            End If
        End If

        If parameter = "eliminar"
            If StudentGrade IsNot Nothing
                Dim Resultado as MsgBoxResult = MsgBoxResult.No
                Resultado = MsgBox("¿Esta seguro de eliminar este registro?",MsgBoxStyle.Question+MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2,"Eliminar")
                if Resultado = MsgBoxResult.Yes
                    DB.StudentGrades.Remove(StudentGrade)
                    DB.SaveChanges()
                    ListStudentGrades = (From S In DB.StudentGrades Select S).ToList()
                    LimpiarControles()
                    notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                    notificacion.TitleText = "Registro Eliminado!"
                    notificacion.ContentText = "El registro se ha eliminado correctamente"
                   notificacion.Popup()
                End If
                Else 
                    MsgBox("Debe de seleccionar un elemento")
            End If
        End If

        
        if parameter = "cancelar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Eliminar = True
            Me.Actualizar = True
            Me.Cancelar = False
            LimpiarControles()
        End If
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
       Return True
    End Function
        Private Sub NotificarCambio(ByVal Propiedad As  String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub
End Class