﻿Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Drawing
Imports Tulpep.NotificationWindow
Imports  MS__SchoolApplication.MSSchoolApplication.JimyDiaz.Model


Public Class DepartmentModelView
    Implements  INotifyPropertyChanged,ICommand,IDataErrorInfo
    Private _Name As String
    Private _Budget As Double
    Private _Administrator As Integer
    Private _Modelo As DepartmentModelView
    Private  _Nuevo as Boolean = true
    Private  _Guardar as Boolean = False
    Private _Cancelar As boolean = False
    Private _Actualizar as Boolean  = True
    Private  _Eliminar As Boolean = True
    'Crea el conjunto de objetos para enlazar los datos y se instancia a la vez
   '
    Private _ListDepartments As New List(Of Department)
    Private  _Element as Department
    Private DB as new MS_SchoolApplicationDataContext
    Dim notificacion as New PopupNotifier

    
    Public  Property  Nuevo() As Boolean
    get 
            Return _Nuevo
    End Get
        Set(value As Boolean)
            _Nuevo = value  
            NotificarCambio("Nuevo")
        End Set
    End Property
     Public  Property Guardar() As Boolean
    get
            Return _Guardar
    End Get
       Set(value As Boolean)
            _Guardar = value
            NotificarCambio("Guardar")
       End Set
   End Property
        Public Property Cancelar() As Boolean
    Get
            Return _Cancelar
    End Get
        Set(value As Boolean)
            _Cancelar = value
            NotificarCambio("Cancelar")
        End Set
    End Property


    Public  Property Actualizar() As Boolean
    Get
            Return  _Actualizar
    End Get
        Set(value As Boolean)
            _Actualizar = value
               NotificarCambio("Actualizar")
        End Set
    End Property

    Public Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value
               NotificarCambio("Eliminar")
        End Set
    End Property


    Public  Property Modelo() As DepartmentModelView
    Get
            Return _Modelo
    End Get
        Set(value As DepartmentModelView)
            _Modelo=value   
        End Set
    End Property



    Public Property  ListDepartments() As List(Of Department)
    Get
           'Verificar qu la lista contenga implicitamente datos cuando se haga el get

            'LINQ usar un alias para obtener los datos
             _ListDepartments = (From D In DB.Departments Select D).ToList()
 
            Return _ListDepartments
    End Get
        Set(value As List(Of Department))
            _ListDepartments = value
            NotificarCambio("ListDepartments")
        End Set
    End Property


    Public  Property  Element() As Department
    get 
            Return _Element
    End Get
        Set(value As Department)
            _Element = value
            NotificarCambio("Element")
            if value IsNot Nothing
                Me.Name = _Element.Name
                Me.Budget = _Element.Budget
                Me.Administrator = _Element.Administrator
            End If
        End Set
    End Property


    Public Property Budget as Double
    Get 
            Return _Budget
    End Get
        Set(value as Double)
            _Budget = value
            NotificarCambio("Budget")
        End Set
    End Property

    Public Property Administrator as Integer
    Get
            Return _Administrator
    End Get
        Set(value as Integer)
            _Administrator = value 
            NotificarCambio("Administrator")
        End Set
    End Property

    Public  Sub  New ()
        Me.Modelo = me
    End Sub
    Public Sub  LimpiarControles()
        Me.Name = ""
        Me.Budget = 0
        Me.Administrator = 0
        Me.Element = Nothing
    End Sub

    'Crear los campos y las propiedades del lado de las clases
    Public  Property  Name() As String
    get
            Return _Name
    End Get
        Set(value As String)
            _Name=value
            NotificarCambio("Name")
        End Set
    End Property
    Public  Sub  NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged (Me,New PropertyChangedEventArgs(Propiedad))
    End Sub

    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
            'descrifrar los errores de las bases de datos
        End Get
    End Property



    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parametro As Object) Implements ICommand.Execute
        'Lógica de la vista va aquí
        If parametro = "nuevo"
            Me.Nuevo = False
            Me.Guardar = True
            Me.Cancelar = True
            Me.Eliminar = False
            Me.Actualizar = False
            LimpiarControles()
        End If

        if parametro = "guardar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Cancelar = False
            Me.Eliminar = True
            Me.Actualizar = True
            Dim Nuevo As New Department
            Nuevo.Name = Me.Name
            Nuevo.Budget = Me.Budget
            Nuevo.StartDate = Date.Now
            Nuevo.Administrator = Me.Administrator
            DB.Departments.Add(Nuevo)
            DB.SaveChanges()
            Me.ListDepartments = (From D in DB.Departments Select D).ToList()
            notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
            notificacion.TitleText = "Registro Realizado!"
            notificacion.ContentText = "El registro se ha realizado correctamente"
            notificacion.Popup()
        End If

        if parametro = "eliminar"
            Try
                  if Element  IsNot Nothing
                Dim Respuesta as MsgBoxResult = MsgBoxResult.No
                Respuesta  = MsgBox("¿Esta seguro de eliminar el registro?",MsgBoxStyle.Question+MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2,"Eliminar")
                If Respuesta = MsgBoxResult.Yes
                    DB.Departments.Remove(Me.Element)
                    DB.SaveChanges()
                    me.ListDepartments = (From D in DB.Departments Select D).ToList()
                    LimpiarControles()
                    notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                      notificacion.TitleText = "Registro Eliminado!"
                     notificacion.ContentText = "El registro se ha eliminado correctamente"
                     notificacion.Popup()
                 End If
                Else 
                 MsgBox("Debe de seleccionar un elemento")
            End If
            Catch ex As Exception
                MsgBox("Este registro tiene relación con otros datos",MsgBoxStyle.Exclamation)
            End Try
          
        End If
        
        if parametro = "actualizar"
            if Element isNot Nothing
                Me.Element.Name = me.Name
                Me.Element.Budget = me.Budget
                Me.Element.Administrator = Me.Administrator
                Me.DB.Entry(Me.Element).State = EntityState.Modified
                Me.DB.SaveChanges()
                ListDepartments = (From D in DB.Departments Select D).ToList()
                notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                notificacion.TitleText = "Registro Actualizado!"
                notificacion.ContentText = "El registro se ha actualizado correctamente"
                notificacion.Popup()
            End If
        End If
        
        if parametro = "cancelar"
            Me.Nuevo=True
            Me.Guardar = False
            Me.Eliminar = True
            Me.Actualizar = True
            Me.Cancelar =False
            LimpiarControles() 
        End If

        if parametro = "modificar" 
            Me.Nuevo=True
            Me.Guardar = False
            Me.Nuevo = False
            Me.Eliminar = False
            me.Actualizar = True
            Me.Cancelar = True
        End If

    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        'Boolean para usar los objetos
        Return true 
    End Function
End Class
