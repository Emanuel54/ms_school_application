﻿Imports  MS__SchoolApplication.MSSchoolApplication.JimyDiaz.Model
Imports System.Data.Entity
Imports System.ComponentModel
Imports System.Drawing
Imports Tulpep.NotificationWindow

Public Class OnsiteCourseModelView
    Implements INotifyPropertyChanged,IDataErrorInfo,ICommand
    #Region "Campos"
    Private _Location as String
    Private _Days as String
    Private _Time as String
    Private _Nuevo as Boolean = True    
    Private _Guardar as Boolean = False
    Private _Eliminar as Boolean =  True
    Private  _Actualizar as Boolean = True
    Private _Cancelar as Boolean = False
    Private _ListCourses As New List(Of Course)
    Private _Course As Course
    Private _OnsiteCourse as OnsiteCourse
    Private  _ListOnsiteCourses as New List(Of OnsiteCourse)
    Private DB as New MS_SchoolApplicationDataContext
    Private _Modelo as OnsiteCourseModelView
    Dim notificacion as New PopupNotifier

#End Region
    #Region "Propiedades"
    Public Property Location() As String
    Get
            Return _Location
    End Get
        Set(value As String)
            _Location = value
            NotificarCambio("Location")
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    Public Property Days() As String
    Get
            Return _Days
    End Get
        Set(value As String)
            _Days = value
            NotificarCambio("Days")
        End Set
    End Property
    Public Property Guardar() As Boolean
    Get
            Return _Guardar
    End Get
        Set(value As Boolean)
            _Guardar = value
            NotificarCambio("Guardar")
        End Set
    End Property
    Public Property  Nuevo() As Boolean
    Get
            Return _Nuevo
    End Get
        Set(value As Boolean)
            _Nuevo = value
            NotificarCambio("Nuevo")
        End Set
    End Property
    Public  Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value
            NotificarCambio("Eliminar")
        End Set
    End Property
    Public  Property Actualizar() As Boolean
    Get
            Return _Actualizar
    End Get
        Set(value As Boolean)
            _Actualizar = value
            NotificarCambio("Actualizar")
        End Set
    End Property
    Public  Property Cancelar() As Boolean
    Get
            Return _Cancelar
    End Get
        Set(value As Boolean)
            _Cancelar = value   
            NotificarCambio("Cancelar")
        End Set
    End Property
    Public Property ListCourses() As List(Of Course)
    Get
            if _ListCourses.Count = 0
                _ListCourses = (From C  in DB.Courses Select C).ToList()
            End If
            Return _ListCourses
    End Get
        Set(value As List(Of Course))
            _ListCourses = value
            NotificarCambio("ListCourses")
        End Set
    End Property
    Public  Property Course() As Course
    Get
            Return _Course
    End Get
        Set(value As Course)
            _Course = value
            NotificarCambio("Course")
        End Set
    End Property


    Public  Property ListOnsiteCourses() As List(Of OnsiteCourse)
    Get
            If _ListOnsiteCourses.Count = 0
                _ListOnsiteCourses = (From O In DB.OnsiteCourses Select O).ToList()
            End If
            Return _ListOnsiteCourses
    End Get
        Set(value As List(Of OnsiteCourse))
            _ListOnsiteCourses= value
            NotificarCambio("ListOnsiteCourses")
        End Set
    End Property

    
    Public  Property OnsiteCourse() As OnsiteCourse
    Get
            Return _OnsiteCourse
    End Get
        Set(value As OnsiteCourse)
            _OnsiteCourse = value
            NotificarCambio("OnsiteCourse")
         If value IsNot Nothing
                Me.Course = _OnsiteCourse.Course
                Me.Location = _OnsiteCourse.Location
                Me.Days = _OnsiteCourse.Days
                Me.Time = _OnsiteCourse.Time
         End If
        End Set
    End Property


    Public Property Time() As String
    Get
            Return _Time
    End Get
        Set(value As String)
            _Time = value
            NotificarCambio("Time")
        End Set
    End Property
    Public  Property Modelo() As OnsiteCourseModelView
    Get
            Return _Modelo
    End Get
        Set(value As OnsiteCourseModelView)
            _Modelo = value
        End Set
    End Property

    Public Sub New ()
        Me.Modelo = Me
    End Sub


    Public Sub LimpiarControles
        Me.Course = Nothing
        Me.Location =""
        Me.Days = ""
        Time = ""
        Me.OnsiteCourse = Nothing
    End Sub
#End Region

    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
       If parameter = "nuevo"
            Me.Nuevo = False
            Me.Guardar = True
            Me.Actualizar = False
            Me.Eliminar = False
            Me.Cancelar = True
            LimpiarControles()
       End If

        If parameter = "guardar"
            Me.Nuevo = True
            Me.Guardar  = false 
            Me.Actualizar = true    
            Me.Eliminar = True
            Me.Cancelar = False
            Dim Nuevo as New OnsiteCourse
               Nuevo.Course = Me.Course
            Nuevo.Location = Me.Location
            Nuevo.Days = Me.Days
            Nuevo.Time = Me.Time
            DB.OnsiteCourses.Add(Nuevo)
            DB.SaveChanges()
            ListOnsiteCourses = (From O in DB.OnsiteCourses Select O).ToList()
               notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
            notificacion.TitleText = "Registro Realizado!"
            notificacion.ContentText = "El registro se ha realizado correctamente"
            notificacion.Popup()
        End If

        If parameter = "actualizar"
            if OnsiteCourse IsNot Nothing
                Me.OnsiteCourse.Location = Me.Location
                Me.OnsiteCourse.Days = Me.Days
                Me.OnsiteCourse.Time = Me.Time
                DB.Entry(OnsiteCourse).State = EntityState.Modified
                DB.SaveChanges()
                ListOnsiteCourses = (From O IN DB.OnsiteCourses Select O).ToList()
                notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                notificacion.TitleText = "Registro Actualizado!"
                notificacion.ContentText = "El registro se ha actualizado correctamente"
                 notificacion.Popup()
            End If
        End If


             if parameter = "eliminar"
            if OnsiteCourse IsNot Nothing
                Dim Resultado as MsgBoxResult = MsgBoxResult.No
                Resultado = MsgBox("¿Está seguro de eliminar este elemento?", MsgBoxStyle.Question +MsgBoxStyle.YesNo +MsgBoxStyle.DefaultButton2, "Eliminar")
                If Resultado = MsgBoxResult.Yes
                    DB.OnsiteCourses.Remove(OnsiteCourse)
                    DB.SaveChanges()
                    ListOnsiteCourses = (From O in DB.OnsiteCourses Select O).ToList()
                    LimpiarControles()
                    notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                    notificacion.TitleText = "Registro Eliminado!"
                    notificacion.ContentText = "El registro se ha eliminado correctamente"
                    notificacion.Popup()
                End If
            End If
        End If



             if parameter = "cancelar"
            Me.Nuevo=True
            Me.Guardar = False
            Me.Eliminar = True
            Me.Actualizar = True
            Me.Cancelar =False 
            LimpiarControles()
        End If

    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me,New PropertyChangedEventArgs(Propiedad))
    End Sub 
End Class
