﻿Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Drawing
Imports Tulpep.NotificationWindow
Imports MS__SchoolApplication.MSSchoolApplication.JimyDiaz.Model


Public Class CourseModelView

#Region "Campos"
    Implements INotifyPropertyChanged, ICommand,IDataErrorInfo
    Private _CourseID As Integer
    Private _Title As String
    Private _Credits As Integer
    Private _Department As Department
    Private _Guardar As Boolean = false
    Private _Nuevo As Boolean = True
    Private _Actualizar As Boolean = True
    Private _Eliminar As Boolean = True
    Private _Cancelar As Boolean = False
    Private  _ListDepartments As New List(Of Department)
    Private _Modelo as CourseModelView
    Private  _ListCourses As New List(Of Course)
    Private  _Course as Course
        Private DB as new MS_SchoolApplicationDataContext
    Dim notificacion as New PopupNotifier
#End Region

#Region "Propiedades"
    Public Property CourseID as Integer 
    Get
            Return _CourseID
    End Get
        Set(value as Integer)
            _CourseID = value
            NotificarCambio("CourseID")
        End Set
    End Property

    Public  Property Title() As String
    Get
            Return _Title
    End Get
        Set(value As String)
            _Title =value
            NotificarCambio("Title")
        End Set
    End Property

    Public  Property Credits() As Integer
    Get
            Return _Credits
    End Get
        Set(value As Integer)
            _Credits = value
            NotificarCambio("Credits")
        End Set
    End Property

    Public  Property Department() As Department
    Get
            Return _Department
    End Get
        Set(value As Department)
            _Department = value
            NotificarCambio("Department")
        End Set
    End Property


    Public Property  Nuevo() As Boolean
    Get
            Return _Nuevo
    End Get
        Set(value As Boolean)
            _Nuevo = value
            NotificarCambio("Nuevo")
        End Set
    End Property
    Public  Property Guardar() As Boolean
    Get
            Return _Guardar
    End Get
        Set(value As Boolean)
            _Guardar = value    
            NotificarCambio("Guardar")
        End Set
    End Property
    Public  Property Actualizar() As Boolean
    Get
            Return _Actualizar
    End Get
        Set(value As Boolean)
            _Actualizar = value
            NotificarCambio("Actualizar")
        End Set
    End Property

    Public Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value
            NotificarCambio("Eliminar")

        End Set
    End Property
    Public Property Cancelar() As Boolean
    Get
            Return _Cancelar
    End Get
        Set(value As Boolean)
            _Cancelar = value
            NotificarCambio("Cancelar")
        End Set
    End Property
    Public Property ListDepartments as List(Of Department)
        Get
                  if _ListDepartments.Count = 0 Then
            'LINQ usar un alias para obtener los datos
             _ListDepartments = (From D In DB.Departments Select D).ToList()
             
        End If
            Return _ListDepartments
    End Get
        Set(value as List(Of Department))
            _ListDepartments = value
            NotificarCambio("ListDepartments")
        End Set
    End Property

    Public  Property ListCourses() As List(Of Course)
    Get
             if _ListCourses.Count = 0 Then
            'LINQ usar un alias para obtener los datos
             _ListCourses = (From C In DB.Courses Select C).ToList()
             
        End If
            Return _ListCourses
    End Get
        Set(value As List(Of Course))
            _ListCourses = value    
            NotificarCambio("ListCourses")
        End Set
    End Property

    Public Property Modelo() As CourseModelView
    Get
            Return _Modelo
    End Get
       Set(value As CourseModelView)
            _Modelo =value
            NotificarCambio("Modelo")
       End Set
   End Property

    Public Sub New ()
        Me.Title=""
        Me.Credits=0
        Me.Modelo=Me
        Me.Department = Nothing
    End Sub
    Public Sub LimpiarControles
        Me.Title=""
        Me.Credits = 0
        Me.Department = Nothing
        Me.Course = Nothing
    End Sub


    Public  Property Course() As Course
    Get
            Return _Course
    End Get
        Set(value As Course)
            _Course = value
            NotificarCambio("Course")
         If value IsNot Nothing
                Me.Title = _Course.Title
                Me.Credits = _Course.Credits
                Me.Department = _Course.Department
         End If
        End Set
    End Property
#End Region
        

    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        If parameter = "nuevo"
            Me.Nuevo = False
            Me.Guardar = True
            Me.Eliminar = False
            Me.Actualizar = False
            Me.Cancelar = True
            LimpiarControles()
        End If

        If parameter = "guardar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Actualizar = True
            Me.Eliminar = True
            Me.Cancelar = False
            Dim Nuevo as New Course
            Nuevo.CourseID = me.CourseID
            Nuevo.Title = me.Title
            Nuevo.Credits = Me.Credits
            Nuevo.Department =   Me.Department
            DB.Courses.Add(Nuevo)
            DB.SaveChanges()
            
            Me.ListCourses = (From C in Db.Courses Select C ).ToList()
            notificacion.TitleFont = New Font("Arial",18,FontStyle.Bold)
            notificacion.TitleText = "Registro Realizado!"
           notificacion.ContentText = "El registro se ha realizado correctamente"
           notificacion.Popup()
        End If

        If parameter = "actualizar"
            if Course IsNot Nothing
                Me.Course.Title = Me.Title
                Me.Course.Credits = Me.Credits
                Me.Course.Department = Me.Department
                Me.DB.Entry(Me.Course).State = EntityState.Modified
                Me.DB.SaveChanges()
                ListCourses = (From C In DB.Courses Select C).ToList()
                 notificacion.TitleFont = New Font("Arial",18,FontStyle.Bold)
                    notificacion.TitleText = "Registro Actualizado!"
                    notificacion.ContentText = "El registro se ha actualizado correctamente"
                    notificacion.Popup()
                Else
                MsgBox("Debe de seleccionar un elemento")
            End If

        End If


     if parameter = "eliminar"
            Try
                 if Course  IsNot Nothing
                Dim Respuesta as MsgBoxResult = MsgBoxResult.No
                Respuesta  = MsgBox("¿Esta seguro de eliminar el registro?",MsgBoxStyle.Question+MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2,"Eliminar")
                If Respuesta = MsgBoxResult.Yes
                    DB.Courses.Remove(Me.Course)
                    DB.SaveChanges()
                    me.ListCourses = (From C in DB.Courses Select C).ToList()
                    LimpiarControles()
                    notificacion.TitleFont = New Font("Arial",18,FontStyle.Bold)
                    notificacion.TitleText = "Registro Eliminado!"
                    notificacion.ContentText = "El registro se ha eliminado correctamente"
                    notificacion.Popup()
                End If
                Else 
                 MsgBox("Debe de seleccionar un elemento")
            End If
            Catch ex As Exception
                MsgBox("Este registro tiene relación con otros datos",MsgBoxStyle.Exclamation)
            End Try
           
        End If

        if parameter = "cancelar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Eliminar = True
            Me.Actualizar = True
            Me.Cancelar = False
            LimpiarControles()
        End If
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function


    Public Sub NotificarCambio(ByVal Propiedad As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(Propiedad))
    End Sub
End Class
