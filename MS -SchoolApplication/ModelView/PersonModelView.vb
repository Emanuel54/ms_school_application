﻿Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Drawing
Imports MS__SchoolApplication.MSSchoolApplication.JimyDiaz.Model
Imports Tulpep.NotificationWindow

Public Class PersonModelView
    Implements  INotifyPropertyChanged,ICommand, IDataErrorInfo
    Private _LastName as String
    Private  _FirstName as String
    Private  _HireDate as Date  'Date.Today.ToString("dd/MM/yyyy")
    Private  _EnrollmentDate as Date  
    Private _Modelo as PersonModelView
    Private _Guardar as Boolean = False  
    Private _Nuevo As Boolean = True    
    Private _Modificar As Boolean = True    
    Private  _Actualizar as Boolean = True
    Private  _Eliminar as Boolean = True
    Private  _Cancelar  as Boolean = False
    Private _ListPersons as New List(Of Person)
    Private _Element as Person
    Private DB as New MS_SchoolApplicationDataContext
    Dim notificacion as New PopupNotifier


    Public  Property LastName as String
    Get
            Return _LastName 
    End Get
        Set(value as String)
            _LastName = value 
            NotificarCambio("LastName")  
        End Set
    End Property

    Public  Property  FirstName() As String
    Get
            return _FirstName
    End Get
        Set(value As String)
            _FirstName = value
            NotificarCambio("FirstName")
        End Set
    End Property

    Public  Property HireDate() As Date
    Get
            Return _HireDate
    End Get
        Set(value As Date)
            _HireDate = value
            NotificarCambio("HireDate")
        End Set
    End Property

    Public  Property EnrollmentDate As Date
    Get
            Return _EnrollmentDate
    End Get
        Set(value As Date)
            _EnrollmentDate = value 
            NotificarCambio("EnrollmentDate")
        End Set
    End Property

    Public  Property Modelo() As PersonModelView
    Get 
            Return  _Modelo
    End Get
        Set(value As PersonModelView)
            _Modelo = value
        End Set
    End Property

    Public Property Guardar as Boolean
    get
            Return _Guardar
    End Get
        Set(value as Boolean)
            _Guardar = value
            NotificarCambio("Guardar")
        End Set
    End Property
    Public  Property  Nuevo() As Boolean
    Get
            Return _Nuevo
    End Get
        Set(value As Boolean)
            _Nuevo = value
            NotificarCambio("Nuevo")
        End Set
    End Property

    Public Property Modificar() As Boolean
    Get
            Return _Modificar
    End Get
        Set(value As Boolean)
            _Modificar = value  
            NotificarCambio("Modificar")
        End Set
    End Property

    Public  Property  Actualizar() As Boolean
    Get
            Return _Actualizar
    End Get
        Set(value As Boolean)
            _Actualizar = value
            NotificarCambio("Actualizar")
        End Set
    End Property

    Public  Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value
            NotificarCambio("Eliminar")
        End Set
    End Property

    Public  Property Cancelar as Boolean
    Get
            Return _Cancelar
    End Get
        Set(value as Boolean)
            _Cancelar = value
            NotificarCambio("Cancelar")
        End Set
    End Property

    Public Property ListPersons() As List(Of Person)
    Get
            if _ListPersons.Count = 0 
                _ListPersons = (From D In DB.Persons Select D).ToList()
            End If
            Return _ListPersons
    End Get
        Set(value As List(Of Person))
            _ListPersons = value
            NotificarCambio("ListPersons")
        End Set
    End Property

    Public  Property Element() As Person
    Get
            Return _Element
    End Get
        Set(value As Person)
            _Element = value
            NotificarCambio("Element")
            if value IsNot Nothing
                Me.LastName = _Element.LastName
                Me.FirstName = _Element.FirstName
                Me.HireDate = _Element.HireDate
                Me.EnrollmentDate = _Element.EnrollmentDate
            End If
        End Set
    End Property


    Public  Sub New ()
        Me.Modelo = Me
        Me.EnrollmentDate = Date.Now
        Me.HireDate = date.Now
    End Sub

    Public Sub LimpiarControles()
        Me.LastName = ""
        Me.FirstName = ""
        Me.HireDate = Date.Now
        Me.HireDate = Date.Now
        Me.Element = Nothing
    End Sub

    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
        If parameter= "nuevo"
            Me.Nuevo = False
            Me.Guardar = True
            Me.Cancelar = True
            Me.Eliminar = False
            Me.Modificar = False
            Me.Actualizar = False
            LimpiarControles()
        End If

        If parameter = "guardar"
                   Me.Nuevo = True
            Me.Guardar = False
            Me.Cancelar = False
            Me.Eliminar = True
            Me.Modificar = True
            Me.Actualizar = True
            Dim Nuevo as New Person
            Nuevo.LastName = Me.LastName
            Nuevo.FirstName = Me.FirstName
            Nuevo.HireDate = Me.HireDate
            Nuevo.EnrollmentDate = Me.EnrollmentDate

            DB.Persons.Add(Nuevo)
            DB.SaveChanges()
            Me.ListPersons = (From D in  DB.Persons Select D).ToList()
            notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
            notificacion.TitleText = "Registro Realizado!"
            notificacion.ContentText = "El registro se ha realizaaod correctamente"
            notificacion.Popup()
        End If



        If parameter = "actualizar"
            if Element IsNot Nothing
                Me.Element.LastName = me.LastName
                Me.Element.FirstName = Me.FirstName
                Me.Element.HireDate = me.HireDate
                Me.Element.EnrollmentDate = Me.EnrollmentDate
                Me.DB.Entry(Me.Element).State = EntityState.Modified
                DB.SaveChanges()
                Me.ListPersons = (From D in DB.Persons Select D).ToList()
                notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                notificacion.TitleText = "Registro Actualizar!"
                notificacion.ContentText = "El registro se ha actualizar correctamente"
                 notificacion.Popup()
            End If
        End If


        if parameter = "eliminar"
            try 
                  if Element IsNot Nothing
                Dim Respuesta as MsgBoxResult = MsgBoxResult.No
                Respuesta = MsgBox("¿Está seguro de eliminar este registro?",MsgBoxStyle.Question+MsgBoxStyle.YesNo+MsgBoxStyle.DefaultButton2,"Eliminar")
                If Respuesta = MsgBoxResult.Yes
                    DB.Persons.Remove(Element)
                    DB.SaveChanges()
                    Me.ListPersons =(From D in Db.Persons Select D).ToList()
                    LimpiarControles()
                    notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                    notificacion.TitleText = "Registro Eliminado!"
                   notificacion.ContentText = "El registro se ha eliminado correctamente"
                   notificacion.Popup()
                End If
                   Else 
                 MsgBox("Debe de seleccionar un elemento")
            End If
            Catch ex As Exception
                 MsgBox("Este registro tiene relación con otros datos",MsgBoxStyle.Exclamation)
            End Try
          
        End If

        if parameter = "cancelar"
             Me.Nuevo=True
            Me.Guardar = False
            Me.Eliminar = True
            Me.Modificar = True
            Me.Actualizar = True
            Me.Cancelar =False
            LimpiarControles()
        End If
        
    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public  Sub NotificarCambio(ByVal Propiedad As String )
        RaiseEvent PropertyChanged(Me,New PropertyChangedEventArgs(Propiedad))

    End Sub
End Class
