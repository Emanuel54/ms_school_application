﻿Imports System.ComponentModel
Imports System.Data
Imports System.Data.Entity
Imports System.Drawing
Imports MS__SchoolApplication.MSSchoolApplication.JimyDiaz.Model
Imports Tulpep.NotificationWindow

Public Class CourseInstructorModelView
    Implements INotifyPropertyChanged, ICommand, IDataErrorInfo

#Region "Campons"
     Private _Course as Course
    Private _Person as Person
    Private _ListCourseInstructors as New List(Of CourseInstructor)
    Private _ListPersons as New  List(Of Person)
    Private _ListCourses as New List(Of Course)
    Private DB As New MS_SchoolApplicationDataContext
    Private _CourseInstructor as CourseInstructor
    Private _Modelo as CourseInstructorModelView
    Private _Guardar As Boolean = false
    Private _Nuevo As Boolean = True
    Private _Eliminar As Boolean = True
    Private _Cancelar As Boolean = False
    Dim notificacion as new PopupNotifier
#End Region
#Region "Propiedades"
    Public  Property Course() As Course
    Get
            Return _Course
    End Get
        Set(value As Course)
            _Course = value 
            NotificarCambio("Course")
        End Set
    End Property
    Public  Property Person as Person
    Get
          Return _Person
    End Get
        Set(value as Person)
            _Person = value
            NotificarCambio("Person")
        End Set
    End Property
    Public Property ListCourseInstructors() As List(Of CourseInstructor)
    Get
            If _ListCourseInstructors.Count = 0
                _ListCourseInstructors = (From C in DB.CourseInstructors Select C).ToList()
            End If
            Return _ListCourseInstructors
    End Get
        Set(value As List(Of CourseInstructor))
            _ListCourseInstructors = value
            NotificarCambio("ListCourseInstructors")
        End Set
    End Property
    Public Property ListPersons() As List(Of Person)
    Get
            If _ListPersons.Count = 0
                _ListPersons = (From P in DB.Persons Select P).ToList() 
            End If

            Return _ListPersons
    End Get
        Set(value As List(Of Person))
            _ListPersons = value
            NotificarCambio("ListPersons")
        End Set
    End Property
    Public  Property ListCourses() As List(Of Course)
    Get
            if _ListCourses.Count = 0
                _ListCourses = (From C In DB.Courses Select C).ToList()
            End If
            Return  _ListCourses
    End Get
        Set(value As List(Of Course))
            _ListCourses = value    
            NotificarCambio("ListCourses")
        End Set
    End Property
    Public Property CourseInstructor() As CourseInstructor
    Get
            Return _CourseInstructor
    End Get
        Set(value As CourseInstructor)
            _CourseInstructor = value
            NotificarCambio("CourseInstructor")
            If value IsNot Nothing
                Me.Course = _CourseInstructor.Course
                Me.Person = _CourseInstructor.Person
            End If
        End Set
    End Property

     Public Property  Nuevo() As Boolean
    Get
            Return _Nuevo
    End Get
        Set(value As Boolean)
            _Nuevo = value
            NotificarCambio("Nuevo")
        End Set
    End Property
    Public  Property Guardar() As Boolean
    Get
            Return _Guardar
    End Get
        Set(value As Boolean)
            _Guardar = value    
            NotificarCambio("Guardar")
        End Set
    End Property
    Public Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value
            NotificarCambio("Eliminar")

        End Set
    End Property

    Public Property Modelo() As CourseInstructorModelView
    Get
            Return _Modelo
    End Get
        Set(value As CourseInstructorModelView)
            _Modelo = value
        End Set
    End Property
    Public Sub New ()
        Me.Modelo = Me
    End Sub
    Public Property Cancelar() As Boolean
    Get
            Return _Cancelar
    End Get
        Set(value As Boolean)
            _Cancelar = value
            NotificarCambio("Cancelar")
        End Set
    End Property

     Public Sub NotificarCambio(ByVal Propiedad  As String)
         RaiseEvent PropertyChanged(Me,New PropertyChangedEventArgs(Propiedad))
     End Sub
    Public  Sub LimpiarControles
        Me.Course = Nothing
        Me.Person = Nothing
        Me.CourseInstructor = Nothing
    End Sub

#End Region

    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
       If parameter = "nuevo"
            Me.Nuevo = False
            Me.Guardar = True
           
            Me.Eliminar = False
            Me.Cancelar = True
            LimpiarControles()
       End If

        if parameter = "guardar"
            Try 
                Me.Nuevo = True 
            Me.Guardar = False
            Me.Eliminar = true
            Me.Cancelar = False
            Dim Nuevo as New CourseInstructor
            Nuevo.Course = Me.Course
            Nuevo.Person = Me.Person
            DB.CourseInstructors.Add(Nuevo)
            DB.SaveChanges()
            ListCourseInstructors = (From C in DB.CourseInstructors Select C).ToList()
            notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
            notificacion.TitleText = "Registro Realizado!"
            notificacion.ContentText = "El registro se ha realizaaod correctamente"
            notificacion.Popup()
            Catch ex As Exception
                   notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                  notificacion.TitleText = "Uppss!!"
                  notificacion.ContentText = "El registro no se ha podido guardar, datos duplicados"
                  notificacion.Popup()
            End Try
            
        End If


        If parameter ="eliminar"
            If CourseInstructor IsNot Nothing
                Dim Respuesta As  MsgBoxResult = MsgBoxResult.No
                Respuesta = MsgBox("¿Desea eliminar este registro?",MsgBoxStyle.Question+MsgBoxStyle.YesNo+MsgBoxStyle.DefaultButton2,"Eliminar")
                If Respuesta = MsgBoxResult.Yes
                    DB.CourseInstructors.Remove(CourseInstructor)
                    DB.SaveChanges()
                    ListCourseInstructors = (From C in DB.CourseInstructors Select C).ToList()
                     notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                      notificacion.TitleText = "Registro Eliminado!"
                    notificacion.ContentText = "El registro se ha eliminado correctamente"
                   notificacion.Popup()
                End If
                Else 
                MsgBox("Debe de seleccionar un registro")
            End If
        End If


        if parameter = "cancelar"
            Me.Nuevo = True 
            Me.Guardar = FAlse  
            Me.Eliminar = True  
            Me.Cancelar = False
        End If

    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True 
    End Function
End Class
