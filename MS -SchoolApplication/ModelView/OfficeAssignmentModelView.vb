﻿Imports  System.ComponentModel
Imports System.Data.Entity
Imports System.Drawing
Imports  MS__SchoolApplication.MSSchoolApplication.JimyDiaz.Model
Imports Tulpep.NotificationWindow
Public Class OfficeAssignmentModelView
    Implements INotifyPropertyChanged, ICommand,IDataErrorInfo
    
    #Region "Campos" 
    Private _Person as Person
    Private _Location As String
    Private  _TimesTamp as Byte
    Private _Guardar As Boolean = False  
    Private  _Nuevo as Boolean = True
    Private  _Actualizar as Boolean =  True 
    Private  _Cancelar As Boolean = False
    Private  _Eliminar as Boolean = True
    Private _OfficeAssignment as OfficeAssignment
    Private  _Modelo as OfficeAssignmentModelView
    Private  _Combobox as Boolean = True
    Private  _ListPersons as New List(Of Person)
    Private  _ListOfficeAssignments As New List(Of OfficeAssignment)
    Private  DB as new MS_SchoolApplicationDataContext
    Dim notificacion as new PopupNotifier
#End Region

    #Region "Propiedades"
    Public Property Person() As Person
    Get
            Return _Person
    End Get
        Set(value As Person)
            _Person = value
            NotificarCambio("Person")
        End Set
    End Property


    Public  Property Location() As String
    Get
            Return _Location
    End Get
        Set(value As String)
            _Location = value
            NotificarCambio("Location")
        End Set
    End Property

    Public  Property TimesTamp() As Byte
    Get
            Return _TimesTamp
    End Get
        Set(value As Byte)
            _TimesTamp =  value
            NotificarCambio("TimesTamp")
        End Set
    End Property

    Public Property Guardar() As Boolean
    Get
            return _Guardar
    End Get
        Set(value As Boolean)
            _Guardar = value
            NotificarCambio("Guardar")
        End Set
    End Property

    Public  Property  Nuevo() As  Boolean
    Get
            Return _Nuevo
    End Get
        Set(value As  Boolean)
            _Nuevo = value
            NotificarCambio("Nuevo")
        End Set
    End Property
    Public  Property  Actualizar() As Boolean
    Get
            Return _Actualizar
    End Get
        Set(value As Boolean)
            _Actualizar = value 
            NotificarCambio("Actualizar")
        End Set
    End Property
    Public  Property Cancelar() As Boolean
    Get
            Return _Cancelar
    End Get
        Set(value As Boolean)
            _Cancelar = value
            NotificarCambio("Cancelar")
        End Set
    End Property


    Public  Property Eliminar() As Boolean
    Get
            Return _Eliminar
    End Get
        Set(value As Boolean)
            _Eliminar = value
            NotificarCambio("Eliminar")
        End Set
    End Property
    Public Property  Combobox() As Boolean
    Get
            Return _Combobox
    End Get
        Set(value As Boolean)
            _Combobox = value
            NotificarCambio("Combobox")
        End Set
    End Property
    Public  Property Modelo() As OfficeAssignmentModelView
    Get
            Return _Modelo 
    End Get
        Set(value As OfficeAssignmentModelView)
            _Modelo = value
            NotificarCambio("Modelo")
        End Set
    End Property

    Public  Property  OfficeAssignment() As OfficeAssignment
    Get
            Return _OfficeAssignment
    End Get
        Set(value As OfficeAssignment)
            _OfficeAssignment = value   
            if value IsNot Nothing
              me.Person = _OfficeAssignment.Person
              me.Location = _OfficeAssignment.Location
              me.TimesTamp = _OfficeAssignment.TimesTamp
            End If
        End Set
    End Property
    Public  Property ListOfficeAssignments as List(Of OfficeAssignment)
    get
        If _ListOfficeAssignments.Count = 0 then
           _ListOfficeAssignments = (From O in DB.OfficeAssignments Select O).ToList()
        End If
            Return _ListOfficeAssignments
    End Get
        Set(value as List(Of OfficeAssignment))
            _ListOfficeAssignments =value
            NotificarCambio("ListOfficeAssignments")
        End Set
    End Property


    Public  Property ListPersons() As List(Of Person)
    Get
        if _ListPersons.Count = 0 
            _ListPersons = (From P In DB.Persons Select P).ToList()
        End If
            Return _ListPersons
    End Get
        Set(value As List(Of Person))
            _ListPersons = value
            NotificarCambio("ListPersons")
        End Set
    End Property


    Public  Sub New 
        Me.Modelo = Me
    End Sub

    Public Sub LimpiarControles
        me.Person = Nothing
        Me.Location = ""
        Me.TimesTamp = 0
        Me.OfficeAssignment = Nothing
    End Sub
#End Region


    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements IDataErrorInfo.Item
        Get
            Throw New NotImplementedException()
        End Get
    End Property

    Public Event CanExecuteChanged As EventHandler Implements ICommand.CanExecuteChanged
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    Public Sub Execute(parameter As Object) Implements ICommand.Execute
       If parameter = "nuevo"
            Me.Nuevo = False
            Me.Guardar  = true  
            Me.Actualizar = False
            Me.Eliminar = False
            Me.Cancelar = True
            Me.Combobox = True
            LimpiarControles()
       End If

        if parameter = "guardar"
            Me.Nuevo = True
            Me.Guardar = False
            Me.Actualizar = True
            Me.Eliminar = True
            Me.Combobox = True
            Me.Cancelar = False
            Dim Nuevo as New OfficeAssignment
            Nuevo.Person = Me.Person
            Nuevo.Location = Me.Location
            Nuevo.TimesTamp = Me.TimesTamp
            DB.OfficeAssignments.Add(Nuevo)
            Db.SaveChanges()
            
            ListOfficeAssignments = (From O in DB.OfficeAssignments  Select O).ToList()
             notificacion.TitleFont = New Font("Arial",18,FontStyle.Bold)
            notificacion.TitleText = "Registro Realizado!"
            notificacion.ContentText = "El registro se ha realizado correctamente"
            notificacion.Popup()
        End If

        If parameter = "actualizar"
            If OfficeAssignment IsNot Nothing
                Me.Combobox = False
                Me.OfficeAssignment.Location = Me.Location
                Me.OfficeAssignment.TimesTamp = Me.TimesTamp
                DB.Entry(Me.OfficeAssignment).State = EntityState.Modified
                DB.SaveChanges()
                ListOfficeAssignments = (From O in Db.OfficeAssignments Select O).ToList()
                notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                notificacion.TitleText = "Registro Actualizado!"
                notificacion.ContentText = "El registro se ha actualizado correctamente"
                notificacion.Popup()
            End If
        End If

        if parameter = "eliminar"
            if OfficeAssignment IsNot Nothing
                Dim Respuesta as MsgBoxResult = MsgBoxResult.No
                Respuesta  = MsgBox("¿Esta seguro de eliminar el registro?",MsgBoxStyle.Question+MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2,"Eliminar")
                if Respuesta = MsgBoxResult.Yes
                    DB.OfficeAssignments.Remove(Me.OfficeAssignment)
                    DB.SaveChanges()
                    ListOfficeAssignments =(From O in DB.OfficeAssignments Select O).ToList
                    LimpiarControles()
                     notificacion.TitleFont = New Font("Arial",18, FontStyle.Bold)
                    notificacion.TitleText = "Registro Eliminado!"
                    notificacion.ContentText = "El registro se ha eliminado correctamente"
                    notificacion.Popup()
                End If
                 Else 
                 MsgBox("Debe de seleccionar un elemento")
            End If
               
        End If


        If parameter = "cancelar"
            Me.Nuevo = True
            Me.Guardar = False  
            Me.Actualizar = True
            Me.Eliminar = true  
            Me.Cancelar = False
            Modelo.Combobox = True  
            LimpiarControles()
        End If

    End Sub

    Public Function CanExecute(parameter As Object) As Boolean Implements ICommand.CanExecute
        Return True
    End Function

    Public  Sub NotificarCambio (ByVal Propiedad As  String)
        RaiseEvent PropertyChanged(Me,New PropertyChangedEventArgs(Propiedad))
    End Sub
End Class
