﻿Imports System.Data.Entity
Imports System.Data.Entity.ModelConfiguration.Conventions
Imports System.Data.SqlClient
Namespace  MSSchoolApplication.JimyDiaz.Model

    Public Class MS_SchoolApplicationDataContext
        Inherits  DbContext
        'Pluralizacion
        Public Property Courses () As DbSet(Of Course)
        Public  Property Departments () As DbSet(Of Department)
        Public  Property OfficeAssignments() As DbSet(Of OfficeAssignment)
        Public Property  OnlineCourses () As DbSet(Of OnlineCourse)
        Public  Property OnsiteCourses() As DbSet(Of OnsiteCourse)
        Public  Property Persons() As DbSet(Of Person)
        Public  Property StudentGrades() As DbSet(Of StudentGrade)
        Public  Property CourseInstructors() As DbSet(Of CourseInstructor)
        
        'Protected Overrides Sub OnModelCreating(Model Builder as DBModelBuilder)
        '       modelBuilder.Conventions.Remove(Of PluralizingTableNameConvention)()
        'Esto quita las pluralizaciones

    End Class
End Namespace
