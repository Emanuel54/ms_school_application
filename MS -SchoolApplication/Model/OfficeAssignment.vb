﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace MSSchoolApplication.JimyDiaz.Model

Public Class OfficeAssignment
#Region "Campos"
    
    Private _InstructorID As Integer
    Private _Location As String
    Private _TimesTamp As Byte
#End Region


#Region "Propiedades"
        Public Overridable Property Person() As Person

        <Key>
        <ForeignKey("Person")>
    Public Property InstructorID As Integer
        Get
            Return _InstructorID
        End Get
        Set(value As Integer)
            _InstructorID = value
        End Set
    End Property

    Public Property Location As String
        Get
            Return _Location
        End Get
        Set(value As String)
            _Location = value
        End Set
    End Property

    Public Property TimesTamp As Byte
        Get
            Return _TimesTamp
        End Get
        Set(value As Byte)
            _TimesTamp = value
        End Set
    End Property
#End Region
End Class
    End Namespace

