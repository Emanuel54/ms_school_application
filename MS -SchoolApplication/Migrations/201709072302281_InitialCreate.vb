Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class InitialCreate
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.CourseInstructors",
                Function(c) New With
                    {
                        .CourseID = c.Int(nullable := False),
                        .PersonID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) New With { t.CourseID, t.PersonID }) _
                .ForeignKey("dbo.Courses", Function(t) t.CourseID, cascadeDelete := True) _
                .ForeignKey("dbo.People", Function(t) t.PersonID, cascadeDelete := True) _
                .Index(Function(t) t.CourseID) _
                .Index(Function(t) t.PersonID)
            
            CreateTable(
                "dbo.Courses",
                Function(c) New With
                    {
                        .CourseID = c.Int(nullable := False, identity := True),
                        .Title = c.String(),
                        .Credits = c.Int(nullable := False),
                        .DepartmentID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.CourseID) _
                .ForeignKey("dbo.Departments", Function(t) t.DepartmentID, cascadeDelete := True) _
                .Index(Function(t) t.DepartmentID)
            
            CreateTable(
                "dbo.Departments",
                Function(c) New With
                    {
                        .DepartmentID = c.Int(nullable := False, identity := True),
                        .Name = c.String(),
                        .Budget = c.Decimal(nullable := False, precision := 18, scale := 2),
                        .StartDate = c.DateTime(nullable := False),
                        .Administrator = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.DepartmentID)
            
            CreateTable(
                "dbo.OnlineCourses",
                Function(c) New With
                    {
                        .CourseID = c.Int(nullable := False),
                        .URL = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CourseID) _
                .ForeignKey("dbo.Courses", Function(t) t.CourseID) _
                .Index(Function(t) t.CourseID)
            
            CreateTable(
                "dbo.OnsiteCourses",
                Function(c) New With
                    {
                        .CourseID = c.Int(nullable := False),
                        .Location = c.String(),
                        .Days = c.String(),
                        .Time = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CourseID) _
                .ForeignKey("dbo.Courses", Function(t) t.CourseID) _
                .Index(Function(t) t.CourseID)
            
            CreateTable(
                "dbo.People",
                Function(c) New With
                    {
                        .PersonID = c.Int(nullable := False, identity := True),
                        .LastName = c.String(),
                        .FirstName = c.String(),
                        .HireDate = c.DateTime(nullable := False),
                        .EnrollmentDate = c.DateTime(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.PersonID)
            
            CreateTable(
                "dbo.OfficeAssignments",
                Function(c) New With
                    {
                        .InstructorID = c.Int(nullable := False),
                        .Location = c.String(),
                        .TimesTamp = c.Byte(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.InstructorID) _
                .ForeignKey("dbo.People", Function(t) t.InstructorID) _
                .Index(Function(t) t.InstructorID)
            
            CreateTable(
                "dbo.StudentGrades",
                Function(c) New With
                    {
                        .EnrollmentID = c.Int(nullable := False, identity := True),
                        .CourseID = c.Int(nullable := False),
                        .StudentID = c.Int(nullable := False),
                        .Grade = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.EnrollmentID) _
                .ForeignKey("dbo.Courses", Function(t) t.CourseID, cascadeDelete := True) _
                .ForeignKey("dbo.People", Function(t) t.StudentID, cascadeDelete := True) _
                .Index(Function(t) t.CourseID) _
                .Index(Function(t) t.StudentID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.CourseInstructors", "PersonID", "dbo.People")
            DropForeignKey("dbo.StudentGrades", "StudentID", "dbo.People")
            DropForeignKey("dbo.StudentGrades", "CourseID", "dbo.Courses")
            DropForeignKey("dbo.OfficeAssignments", "InstructorID", "dbo.People")
            DropForeignKey("dbo.CourseInstructors", "CourseID", "dbo.Courses")
            DropForeignKey("dbo.OnsiteCourses", "CourseID", "dbo.Courses")
            DropForeignKey("dbo.OnlineCourses", "CourseID", "dbo.Courses")
            DropForeignKey("dbo.Courses", "DepartmentID", "dbo.Departments")
            DropIndex("dbo.StudentGrades", New String() { "StudentID" })
            DropIndex("dbo.StudentGrades", New String() { "CourseID" })
            DropIndex("dbo.OfficeAssignments", New String() { "InstructorID" })
            DropIndex("dbo.OnsiteCourses", New String() { "CourseID" })
            DropIndex("dbo.OnlineCourses", New String() { "CourseID" })
            DropIndex("dbo.Courses", New String() { "DepartmentID" })
            DropIndex("dbo.CourseInstructors", New String() { "PersonID" })
            DropIndex("dbo.CourseInstructors", New String() { "CourseID" })
            DropTable("dbo.StudentGrades")
            DropTable("dbo.OfficeAssignments")
            DropTable("dbo.People")
            DropTable("dbo.OnsiteCourses")
            DropTable("dbo.OnlineCourses")
            DropTable("dbo.Departments")
            DropTable("dbo.Courses")
            DropTable("dbo.CourseInstructors")
        End Sub
    End Class
End Namespace
